<?php 
session_start();
if(!isset($_SESSION['usuario'])) {
	header('Location: login.php'); 
	exit();
}

include_once("header.php");
?>

<section id="breadcrumb">
	<div class="container">
		<ol class="breadcrumb">
			<li class="active">Dashboard</li>
		</ol>
	</div>
</section>

<section class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="list-group">
					<a href="index.html" class="list-group-item active main-color-bg">
						<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Dashboard
					</a>
					<a href="#" onclick="clickealo();return false" class="list-group-item"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Unidades <span class="badge">44</span></a>
				</div>

				<div class="well">
					<h4>Disk space used</h4>
					<div class="progress">
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
								60%
							</div>
						</div>
					</div>
				</div>

				<div class="well">
					<h4>Bandwidth used</h4>
					<div class="progress">
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
								60%
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-heading main-color-bg">
						<h3 class="panel-title">Website Overview</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-3">
							<div class="well dash-box">
								<h2><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> 203</h2>
								<h4>Pages</h4>
							</div>
						</div>
						<div class="col-md-3">
							<div class="well dash-box">
								<h2><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> 203</h2>
								<h4>Posts</h4>
							</div>
						</div>
						<div class="col-md-3">
							<div class="well dash-box">
								<h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span> 203</h2>
								<h4>Users</h4>
							</div>
						</div>
						<div class="col-md-3">
							<div class="well dash-box">
								<h2><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> 203</h2>
								<h4>Visitors</h4>
							</div>
						</div>
					</div>

				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Users</h3>
					</div>
					<div class="panel-body">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Added</th>
									<th>Option</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Juan</td>
									<td>23-02-2000</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Jose</td>
									<td>09-06-2001</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Juan</td>
									<td>23-02-2000</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Jose</td>
									<td>09-06-2001</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Juan</td>
									<td>23-02-2000</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Jose</td>
									<td>09-06-2001</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Juan</td>
									<td>23-02-2000</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Jose</td>
									<td>09-06-2001</td>
									<td>
										<a href="#">Eliminar</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<?php include_once("footer.php"); ?>