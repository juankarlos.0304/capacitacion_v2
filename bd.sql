CREATE TABLE IF NOT EXISTS users (
	id int(10) NOT NULL AUTO_INCREMENT,
	email varchar(100) DEFAULT NULL,
	name varchar(50) NOT NULL,
	password varchar(50) NOT NULL,
	type varchar(50) NOT NULL,
	meta_id int(10) DEFAULT NULL,
	PRIMARY KEY(id)
) ENGINE=InnoDB;

INSERT INTO users (id, email, name, password, type, meta_id) VALUES
(1, "admin@css.com", 'adm', '123', 'Admin', 0);

CREATE TABLE IF NOT EXISTS unidad (
	id int(10) NOT NULL AUTO_INCREMENT,
	nombre varchar(50) NOT NULL,
	email varchar(50) DEFAULT NULL,
	telefono varchar(20) DEFAULT NULL,
	localidad varchar(50) DEFAULT NULL,
	municipio varchar(50) DEFAULT NULL,
	estado varchar(50) DEFAULT NULL,
	actividad varchar(200) DEFAULT NULL,
	logo varchar(100) DEFAULT NULL,
	PRIMARY KEY(id)
) ENGINE=InnoDB;

INSERT INTO `unidad` (`id`, `nombre`, `email`, `telefono`, `localidad`, `municipio`, `estado`, `actividad`, `logo`) VALUES (NULL, 'unidad01', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `unidad` (`id`, `nombre`, `email`, `telefono`, `localidad`, `municipio`, `estado`, `actividad`, `logo`) VALUES (NULL, 'unidad02', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `unidad` (`id`, `nombre`, `email`, `telefono`, `localidad`, `municipio`, `estado`, `actividad`, `logo`) VALUES (NULL, 'unidad03', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `unidad` (`id`, `nombre`, `email`, `telefono`, `localidad`, `municipio`, `estado`, `actividad`, `logo`) VALUES (NULL, 'unidad04', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `unidad` (`id`, `nombre`, `email`, `telefono`, `localidad`, `municipio`, `estado`, `actividad`, `logo`) VALUES (NULL, 'unidad05', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
