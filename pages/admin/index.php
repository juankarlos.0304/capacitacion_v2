<div class="panel panel-default">
	<div class="panel-heading main-color-bg">
		<h3 class="panel-title">Unidades</h3>
	</div>
	<div class="panel-body">
		
		<form class="form-horizontal">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-6">
					<input type="email" class="form-control" id="inputEmail3" placeholder="Email" list="miLista">
				</div>
				<div class="col-sm-4">
					<div class="">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Agregar</button>
					</div>
				</div>
			</div>
		</form>

		<div id="content_unidad">
			<table class="table table-stripped table-bordered">
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Municipio</th>
					<th>Estado</th>
					<th>Actividad</th>
					<th></th>
				</tr>
				<?php require_once("../../core/data.get.php");
					$res = $get->get_data_array("select * from unidad");
					foreach ($res as $key) {
						echo "<tr>";
						echo "<td>".$key['id']."</td>";
						echo "<td><a href='#'>".$key['nombre']."</a></td>";
						echo "<td>".$key['municipio']."</td>";
						echo "<td>".$key['estado']."</td>";
						echo "<td>".$key['actividad']."</td>";
						echo "<td>
									<a href='".$key['id']."' onclick='eliminar_unidad(".$key['id'].");return false'>Eliminar</a>
									<a href='".$key['id']."' onclick='editar_unidad(".$key['id'].");return false'>Editar</a>
								</td>";
						echo "</tr>";
					}
				?>
			</table>
		</div>

	</div>
</div>


<datalist id="miLista">
<?php 
foreach ($res as $key) {
	echo "<option>".$key['nombre']."</option>";
}
?>
</datalist>



<!-- Modal Nuevo -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar</h4>
			</div>
			<div class="modal-body">

				<form action="#" id="formunidad" onsubmit="guardarunidad();return false">
					<div class="form-group">
						<label for="">Email</label>
						<input type="email" class="form-control" name="u_Email" id="u_Email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" class="form-control" name="u_Nombre" id="u_Nombre" placeholder="Nombre">
					</div>
					<div class="form-group">
						<label for="">Telefono</label>
						<input type="text" class="form-control" name="u_Telefono" id="u_Telefono" placeholder="Telefono">
					</div>
					<div class="form-group">
						<label for="">Localidad</label>
						<input type="text" class="form-control" name="u_Localidad" id="u_Localidad" placeholder="Localidad">
					</div>
					<div class="form-group">
						<label for="">Municipio</label>
						<input type="text" class="form-control" name="u_Municipio" id="u_Municipio" placeholder="Municipio">
					</div>
					<div class="form-group">
						<label for="">Estado</label>
						<input type="text" class="form-control" name="u_Estado" id="u_Estado" placeholder="Estado">
					</div>
					<div class="form-group">
						<label for="">Actividad</label>
						<input type="text" class="form-control" name="u_Actividad" id="u_Actividad" placeholder="Actividad">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>

			</div>
		</div>
	</div>
</div>


<!-- Modal Editar -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Editar</h4>
			</div>
			<div class="modal-body modal-body_editar">

				e

			</div>
		</div>
	</div>
</div>