<?php require_once("../../core/data.get.php"); 
$res = $get->geta_data_from_id("unidad",$_POST['idu']);
foreach ($res as $key) { ?>

<form action="#" id="formunidad" onsubmit="actualizarunidad();return false">

	<input type="hidden" value="<?php echo $key['id']; ?>" name="up_id" id="up_id">
	<div class="form-group">
		<label for="">Email</label>
		<input type="email" class="form-control" name="u_Email" id="up_Email" value="<?php echo $key['email']; ?>">
	</div>
	<div class="form-group">
		<label for="">Nombre</label>
		<input type="text" class="form-control" name="u_Nombre" id="up_Nombre" value="<?php echo $key['nombre']; ?>">
	</div>
	<div class="form-group">
		<label for="">Telefono</label>
		<input type="text" class="form-control" name="u_Telefono" id="up_Telefono" value="<?php echo $key['telefono']; ?>">
	</div>
	<div class="form-group">
		<label for="">Localidad</label>
		<input type="text" class="form-control" name="u_Localidad" id="up_Localidad"value="<?php echo $key['localidad']; ?>">
	</div>
	<div class="form-group">
		<label for="">Municipio</label>
		<input type="text" class="form-control" name="u_Municipio" id="up_Municipio" value="<?php echo $key['municipio']; ?>">
	</div>
	<div class="form-group">
		<label for="">Estado</label>
		<input type="text" class="form-control" name="u_Estado" id="up_Estado" value="<?php echo $key['estado']; ?>">
	</div>
	<div class="form-group">
		<label for="">Actividad</label>
		<input type="text" class="form-control" name="u_Actividad" id="up_Actividad" value="<?php echo $key['actividad']; ?>">
	</div>
	<button type="submit" class="btn btn-default">Actualizar</button>
</form>

<?php } ?>