<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Starter Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="assets/css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Admin</a>
		</div>

		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.html">Dashboard</a></li>
				<li><a href="pages.html">Pages</a></li>
				<li><a href="posts.html">Posts</a></li>
				<li><a href="user.html">User</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Welsome Brad</a></li>
				<li><a href="core/session.logout.php">Logout</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>

<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h1>Dashboard <small><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Manage your site</small></h1>
			</div>
			<div class="col-md-2">
				<div class="dropdown create">
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Create content
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="#">Add Page</a></li>
						<li><a href="#">Add Post</a></li>
						<li><a href="#">Add User</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>